#include<stdio.h>
int main()
{
	int a;
	int b;
	int temp;
	
	printf("Enter two numbers to swap\n");
	scanf("%d%d",&a,&b);
	
	printf("Before swapping a=%d\n",a);
	printf("Before swapping b=%d\n",b);
	
	temp=a;
	a=b;
	b=temp;
	
	printf("After swapping a=%d\n",a);
	printf("After swapping b=%d\n",b);
	
	return 0;
	
	
}
